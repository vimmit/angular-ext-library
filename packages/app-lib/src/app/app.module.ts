import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MyUiLibModule } from 'projects/my-ui-lib/src/public_api';

import { CoolUiLibModule } from 'cool-ui-lib';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CoolUiLibModule,
    MyUiLibModule,
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
