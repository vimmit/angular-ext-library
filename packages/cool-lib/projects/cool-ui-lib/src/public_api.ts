/*
 * Public API Surface of cool-ui-lib
 */

export * from './lib/cool-ui-lib.service';
export * from './lib/cool-ui-lib.component';
export * from './lib/cool-ui-lib.module';
