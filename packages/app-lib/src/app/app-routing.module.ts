import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyUiLibComponent } from 'projects/my-ui-lib/src/public_api';

const routes: Routes = [
  {
    path: '', component: MyUiLibComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
