import { NgModule } from '@angular/core';
import { CoolUiLibComponent } from './cool-ui-lib.component';

@NgModule({
  declarations: [CoolUiLibComponent],
  imports: [
  ],
  exports: [CoolUiLibComponent]
})
export class CoolUiLibModule { }
