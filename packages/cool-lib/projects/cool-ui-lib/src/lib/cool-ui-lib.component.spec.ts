import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoolUiLibComponent } from './cool-ui-lib.component';

describe('CoolUiLibComponent', () => {
  let component: CoolUiLibComponent;
  let fixture: ComponentFixture<CoolUiLibComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoolUiLibComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoolUiLibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
