## Installation

Run `npm run bootstrap`. Ironically not with lerna.

## App-lib

`cd packages/app-lib`

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Cool-lib

`cd packages/cool-lib`

Run `npm run pack:watch` for a packager with the watch option. The app will automatically reload if you change any of the source files.

If you don't want to watch.
Run `npm run pack`
