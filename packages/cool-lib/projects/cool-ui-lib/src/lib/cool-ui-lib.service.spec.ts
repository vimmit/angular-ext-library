import { TestBed } from '@angular/core/testing';

import { CoolUiLibService } from './cool-ui-lib.service';

describe('CoolUiLibService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CoolUiLibService = TestBed.get(CoolUiLibService);
    expect(service).toBeTruthy();
  });
});
